#ifndef CONFERENCE_PROGRAM_H
#define CONFERENCE_PROGRAM_H

#include "constants.h"

struct timef
{
    string hours;
    string minutes;
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct conference_program
{
    person reader;
    timef start;
    timef finish;
    person author;
    char title[MAX_STRING_SIZE];
};

#endif