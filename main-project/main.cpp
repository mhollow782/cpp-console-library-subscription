#include <iostream>
#include <iomanip>

using namespace std;

#include "conference_program.h"
#include "file_reader.h"
#include "constants.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "Лабораторная работа №8. GIT\n";
    cout << "Вариант №2. Программа конференций\n";
    cout << "Автор: Багдя Павел\n\n";
    cout << "Группа: 11\n";
    conference_program* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "***** Библиотечный абонемент *****\n\n";
        for (int i = 0; i < size; i++)
        {
            cout << "Докладчик........: ";
            cout << subscriptions[i]->reader.last_name << " ";
            cout << subscriptions[i]->reader.first_name[0] << ". ";
            cout << subscriptions[i]->reader.middle_name[0] << ".";
            cout << '\n';
            cout << "Доклад...........: ";
            cout << subscriptions[i]->author.last_name << " ";
            cout << subscriptions[i]->author.first_name[0] << ". ";
            cout << subscriptions[i]->author.middle_name[0] << ".";
            cout << ", ";
            cout << '"' << subscriptions[i]->title << '"';
            cout << '\n';
            cout << "Время начала.....: ";
            cout << setw(4) << setfill('0') << subscriptions[i]->start.hours << '-';
            cout << setw(2) << setfill('0') << subscriptions[i]->start.minutes << '-';
            cout << '\n';
            cout << "Время конца...: ";
            cout << setw(4) << setfill('0') << subscriptions[i]->finish.hours << '-';
            cout << setw(2) << setfill('0') << subscriptions[i]->finish.minutes << '-';
            cout << '\n';
            cout << '\n';
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}